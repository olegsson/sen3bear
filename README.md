# #sen3bear

Team __sen3bear__'s repository for [Copernicus Hackathon Zagreb 2019](https://www.copernicus-zagreb.eu/eng.html): a web app for monitoring the state of [Nature Park Medvednica](https://www.pp-medvednica.hr/en/).

![](./assets/sen3bear.gif)
