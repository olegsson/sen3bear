import mercantile
import rasterio as rio
from rio_tiler import main
from glob import glob
from hashlib import sha256
import numpy as np
from pathlib import Path

try:
    from . import settings
except ImportError:
    from sen3back import settings

def get_tile(resource, x, y, z):
    tilename = sha256('-'.join(map(str, (resource, x, y, z))).encode()).hexdigest()
    tilefile = settings.tilecache_dir.joinpath(tilename+'.npy')
    if tilefile.exists():
        return np.load(tilefile)
    tile = main.tile(resource, x, y, z)[0] / 10000
    np.save(tilefile, tile)
    return tile

if __name__ == '__main__':
    from sen3back import tilefuncs
    from sen3back import rendering

    z, x, y = 14, 8918, 5834

    tif = 'file://' + next(settings.raster_dir.glob('*.tif')).as_posix()
    tile = get_tile(tif, x, y, z)


    tile = tilefuncs.func_ndvi(tile)
    tile = rendering.render(tile)
    with open('tmp.png', 'wb') as f:
        f.write(tile)
