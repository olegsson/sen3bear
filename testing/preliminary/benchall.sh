#/usr/bin/env bash

go run httpfunnel.go -p 8000 -d sockets &
./start_api.sh point &
./start_api.sh tile &
./start_api.sh tilecomp &

for test in rio_point rio_tile rio_tilecomp flask_point flask_tile flask_tilecomp
do
    echo running $test
    ./benchit.sh $test
done

pkill -f bench:app
pkill -f httpfunnel
