#%%
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn import model_selection, metrics
import numpy as np
from pathlib import Path
from sklearn.preprocessing import LabelEncoder
import pickle
from traingen import ChooChooTrain
import rasterio as rio

fld = Path('/data/hackathon/train')
fld_nn = fld/'xg'

prefix='clc8xg'
#%%


fn_data = fld/'mosaics_data.npz'
# data = np.load(fn_data)
with np.load(fn_data) as data:
    y = data['y']
    #ind = sample_classes_idx(y, [0.5,0.4,1,1,0.5,0.2,1,1])
    #y=y[ind]
    x = data['x'][:,:-1]#[ind]
    season = data['season']#[ind]

random_state=42
train_size=0.8
test_size=0.1
validation_size=0.1
indices = np.arange(x.shape[0])
idx_train, idx_test = train_test_split(indices, test_size=test_size, 
    random_state=random_state, shuffle=True, stratify=y)

x_train = np.c_[x[idx_train], season[idx_train]]
y_train = y[idx_train]
x_test = np.c_[x[idx_test], season[idx_test]]
y_test = y[idx_test]
#%%eval_metric = ["merror", "map", "auc"]
def train():
    ver='3'
    # fit model no training data
    model = XGBClassifier(objective = "multi:softmax", nthread=12,
        n_estimators=500, learning_rate = 0.01)#, tree_method = "gpu_exact", predictor = "gpu_predictor")
    model.fit(x_train, y_train, eval_set=[(x_test, y_test)], 
        eval_metric=['merror','mlogloss'], early_stopping_rounds = 10, 
        verbose=True)

    fn = str(fld_nn/f'{prefix}_{ver}.model')
    pickle.dump(model, open(fn, "wb"))

    #model.save_model())
    #model.dump_model(str(fld_nn/f'{prefix}_{ver}.raw.txt'))

#%%
def test():
    prefix='clc8xg'
    ver='2'
    #model = XGBClassifier({'nthread':4}) #init model
    fn = str(fld_nn/f'{prefix}_{ver}.model')
    model = pickle.load(open(fn, "rb"))

    prd_test = model.predict(x_test)

    report = metrics.classification_report(y_test, prd_test)
    confusion = metrics.confusion_matrix(y_test, prd_test)
    print(report)
    print(confusion)

    sf = fld_nn/f'{prefix}_{ver}_stats.txt'
    sf.write_text(f'{report}\n{repr(confusion)}')

def prediction(ver, year, season):
    #fn_input = r'D:\DATA\HACKATON\mosaics\season\mosaic_2018_s2.tif'
    #fn_input = '/data/hackathon/mosaics/season/mosaic_2018_s2.tif'
    fn_input = f'/data/hackathon/mosaics/season/mosaic_{year}_s{season}.tif'
    #fn_roi = r'D:\DATA\HACKATON\mosaics\roi\ROI_10am.shp'
    fn_roi = '/data/hackathon/ROI_10am.shp'
    msp = ChooChooTrain.from_msp_1file(fn_input, fn_roi, 'ID', 0)

    nclasses=8
    img = np.zeros(msp.input.mask.shape, dtype='uint8').reshape(-1)#.reshape((-1,nclasses+1))

    fn = str(fld_nn/f'{prefix}_{ver}.model')
    model = pickle.load(open(fn, "rb"))

    prdsteps, gprd = msp.prediction(batch_size=100000)
    for i in range(prdsteps):
        if i%10==0:
            print(f'{i}/{prdsteps}')
        idx, xx = next(gprd)
        xx = xx.squeeze()#[:,:-1]
        xx = np.c_[xx, np.zeros((xx.shape[0],1))+season]
        prd = model.predict(xx)#.numpy()
        #img[idx,1:] = prd*100  
        #prd = prd.argmax(axis=-1)
        img[idx] = prd #msp.target.encoder.inverse_transform(prd)

    img = img.reshape(msp.input.mask.shape)
    profile = rio.default_gtiff_profile
    profile.update(nodata=0, dtype='uint8', transform=msp.input.transform, crs=msp.input.crs,
        width=img.shape[1], height=img.shape[0], count=1)

    outfile = fld/f'prd_{prefix}_v{ver}_{year}_s{season}.tif'
    with rio.open(outfile, mode='w', **profile) as r:
        r.write(img.astype(rio.uint8),1)
        #for i in np.arange(nclasses+1):
        #    r.write(img[:,:,i].astype(rio.uint8),int(i+1))
#%%
if __name__=='__main__':
    train()

# %%
