
## Tools/packages

### Back end

**python**:
* rasterio
* numpy
* pandas (geopandas)
* <s>Matplotlib</s> (ne treba nam ako radimo grafove u frontendu, jedino možda za colormaps)
* Flask (web server)
* GeoPySpark (https://geopyspark.readthedocs.io/en/latest/)
 - on-the-fly računanje s rasterima, builtin TMS, scalable jer je na sparku, čini se idealno
 - +spark je buzzword tak da bonus bodovi :)


**DB**
* postGIS
 - ako koristimo GeoPySpark možda možemo ovo izbjeći (ili koristiti samo za vektore)


### Front end

**core:**
* vue.js
* bootstrap (css part only)


**charts:**
* vuebars/vue trend
 - *(jednostavan chart library, svi parametri se predaju kroz vue props property)*
 - *primjer:* https://jsfiddle.net/mpavisic2/vpxn5f4h/


* chart.js (dostupan vue.js wrapper)
 - *podržava naprednije vizualizacije, više na linku: https://www.chartjs.org/samples/latest/*


**kartografija**
* leaflet
 - +milijardu pluginova koji će nam vjerojatno trebati
