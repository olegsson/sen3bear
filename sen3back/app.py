from flask import Flask, Response, abort, request
from rio_tiler.errors import TileOutsideBounds
from random import choice
import diskcache
import requests
from PIL import Image
from io import BytesIO
import numpy as np

from . import settings, tiling, tilefuncs, rendering, utils

api_state = Flask('api_state')
api_basemap = Flask('api_basemap')
api_diff = Flask('api_diff')
api_maps = Flask('api_maps')
api_legend = Flask('api_legend')

basemap_cache = diskcache.Cache()

@api_basemap.route('/<int:z>/<int:x>/<int:y>')
def serve_basemap(z, x, y):
    tilename = f'/toner-hybrid/{z}/{x}/{y}.png'
    tile = basemap_cache.get(tilename)
    if tile is None:
        stamen_url = f'http://{choice("abcd")}.tile.stamen.com' + tilename
        tile = requests.get(stamen_url).content
        basemap_cache.set(tilename, tile)
    return Response(tile, mimetype='image/png')

@api_state.route('/<time>/<func>/<int:z>/<int:x>/<int:y>')
def serve_state(time, func, z, x, y):
    if func == 'light':
        try:
            resource = 'file://' + settings.light_tif.as_posix()
            tile = tiling.get_tile(resource, x, y, z)[utils.time2idx(time)] * 10000
            tile = rendering.render(tile, 0., 100., 'light')
        # print(tile)

        except TileOutsideBounds:
            abort(404)
    else:
        try:
            resource = 'file://' + next(settings.raster_dir.glob(f'*{time}*.tif')).as_posix()
            tile = tiling.get_tile(resource, x, y, z)
            tilestuff = tilefuncs.__dict__['func_'+func](tile)
        except (StopIteration, TileOutsideBounds, KeyError):
            abort(404)

        # print('state', func, tile.min(), tile.max(), tile.mean())

        tile = rendering.render(*tilestuff)
    return Response(tile, mimetype='image/png')

@api_diff.route('/<time1>/<time2>/<func>/<int:z>/<int:x>/<int:y>')
def serve_diff(time1, time2, func, z, x, y):
    if time2 < time1: abort(404)
    try:
        resource1 = 'file://' + next(settings.raster_dir.glob(f'*{time1}*.tif')).as_posix()
        resource2 = 'file://' + next(settings.raster_dir.glob(f'*{time2}*.tif')).as_posix()
        func = tilefuncs.__dict__['func_'+func]
        tile1 = tiling.get_tile(resource1, x, y, z)
        tile2 = tiling.get_tile(resource2, x, y, z)
        tile1, vmin, vmax, cramp = func(tile1)
        tile2, *__ = func(tile2)
    except (StopIteration, TileOutsideBounds, KeyError):
        abort(404)

    # time1, time2 = map(utils.timestamp2year, (time1, time2))
    tile = (tile2 - tile1) #/ (time2 - time1)
    drange = vmax - vmin

    # print('diff', func, tile.min(), tile.max(), tile.mean())

    tile = rendering.render(tile, -drange, drange, 'diff_'+cramp)
    return Response(tile, mimetype='image/png')

@api_maps.route('/<map>/<int:z>/<int:x>/<int:y>')
def serve_map(map, z, x, y):
    try:
        resource = 'file://' + next(settings.maps_dir.glob(f'{map}.tif*')).as_posix()
        tile = tiling.get_tile(resource, x, y, z)
        tile = np.stack(tile, -1)
        tile = (tile * 10000).astype(np.uint8)
        tile = Image.fromarray(tile, mode='RGBA')
        buffer = BytesIO()
        tile.save(buffer, format='PNG')
        return Response(buffer.getvalue(), mimetype='image/png')
    except (StopIteration, TileOutsideBounds):
        abort(404)

@api_legend.route('/<map>')
def serve_legend(map):
    try:
        with open(next(settings.maps_dir.glob(f'{map}.png')), 'rb') as f:
            return Response(f.read(), mimetype='image/png')
    except StopIteration:
        abort(404)
