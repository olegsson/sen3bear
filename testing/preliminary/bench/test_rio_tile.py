from glob import glob
import cProfile
from shapely import speedups
if speedups.available:
    speedups.enable()
from shapely.geometry import Point, Polygon, box
import numpy as np
import rasterio as rio
from rasterio.windows import from_bounds
from rasterio.enums import Resampling
from random import choice

from .test_rio_point import random_coords
from . import settings

def get_tile_data(src, pt):
    windim = min(src.width, src.height) * src.transform[0] * .5 * np.random.uniform()
    win = from_bounds(
        pt[0],
        pt[1],
        pt[0]+windim,
        pt[1]+windim,
        transform=src.transform,
    )
    return src.read(
        1, window=win, out_shape=(256, 256),
        resampling=Resampling.bilinear,
    )

def test():
    src = rio.open(choice(settings.raster_files()))
    pt = random_coords(src)
    tile = get_tile_data(src, pt)
    return tile

if __name__ == '__main__':
    print(test())
