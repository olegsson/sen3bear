#/usr/bin/env bash

gunicorn bench:app_$1 -w 8 --bind=unix:$PWD/sockets/$1 --log-level debug
