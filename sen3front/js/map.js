
do_it = () => {
function myFunction() {
  console.log('++');
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
/*var point_xy=[51.505, -0.09];
var map = L.map('mapid').setView(point_xy, 13);


L.tileLayer(
  'http://{s}.tile.osm.org/{z}/{x}/{y}.png',//'http://{s}.tile.stamen.com/toner-hybrid/{z}/{x}/{y}.png', {
  {  maxZoom: 18
  }).addTo(map);

L.marker(point_xy).addTo(map)
    .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
    .openPopup();
    //Vue.component('l-map', window.Vue2Leaflet.LMap);
    //Vue.component('l-tile-layer', window.Vue2Leaflet.LTileLayer);
  //  Vue.component('l-marker', window.Vue2Leaflet.LMarker);*/
  var customDefault = L.icon({
  			iconUrl: 'images/marker-icon.png',
  			shadowUrl: 'images/marker-shadow.png',
  		});
var { LMap, LTileLayer, LMarker } = Vue2Leaflet;

    new Vue({
      el: '#app',
      components: { LMap, LTileLayer, LMarker },
      data() {
        return {
          zoom:13,
          center: L.latLng(45.911942, 15.958893),
          url:'http://'+window.location.hostname+':5000/state/2019_s3/ndvi/{z}/{x}/{y}',//{s}.tile.stamen.com/toner-hybrid/{z}/{x}/{y}.png',//'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
          attribution:'&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
          markers: [L.latLng(45.911942, 15.958893),],
          sfilter: "2018_s1",
          sfilter2: "2018_s4",
          labelkey: "seasonal_trend",
          tempCompare: false,
          slideBreaks:9,
          show_legend: false,
          sliderLabels: [{'seasonal_trend':
          [
            "2016_s1",
            "2016_s2",
            "2016_s3",
            "2016_s4",
            "2017_s1",
            "2017_s2",
            "2017_s3",
            "2017_s4",
            "2018_s1",
            "2018_s2",
            "2018_s3",
            "2018_s4",]},{
            'light_polution':[
              '201601',
'201602',
'201603',
'201604',
'201605',
'201606',
'201607',
'201608',
'201609',
'201610',
'201611',
'201612',
'201701',
'201702',
'201703',
'201704',
'201705',
'201706',
'201707',
'201708',
'201709',
'201710',
'201711',
'201712',
'201801',
'201802',
'201803',
'201804',
'201805',
'201806',
'201807',
'201808',
'201809',
'201810',
'201811',
'201812',


            ]}]

            ,
          rbLayers: ['ndvi', 'evi', 'ari', 'slavi', 'light',],
          singleMaps:['beskraljesnjaci',
          'fauna_sve',
          'flora_invazivne_vrste',
          'flora_sve',
          'flora_ugrozene_vrste',
          'gmazovi',
          'ptice',
          'riba',
          'sisavci_osim-sismisa',
          'sismisi',
          'stanje_voda',
          'vodozemci',
          ],
          acLayer: 'ndvi',
          //hvatanje slider pozicija
          slider_1: 0,
          slider_2: 0,
          survey:
          [{title:'Bjuti faktor:','qa':[1,2,3,4,5]},
          {title:'Razlog posjete:',
qa:['samo prolazim',
'ležerna šetnja',
'punjenje baterija (stress-relief)',
'inspiracija',
'kontakt s prirodom',
'rekreacija (planinarenje)']},
{title:'Uočeni pritisci:',
qa:['nepropisni otpad',
'nepropisno parkiranje',
'previše ljudi (prevelika buka)'
]},{title:'Piši nam (prostor za dodatan komentar):',
qa:['nepropisni otpad',
'nepropisno parkiranje',
'previše ljudi (prevelika buka)']}]



        }
      },
      methods:{
        catchEvent: function(e){
          this.markers.pop(0);
          this.markers.push(e.latlng);//push(e.latlng);
        },
        /*definekey: function(key){
          listArray=[]


          if (key==='trend'){
            return 0;
          } else if (key==='seasonal')
          {
            return 0;
          } else (key==='light-polution')
          {
            return 1;
          }
        }*/

      slideFilter: function(e){

        key=this.labelkey;
        this.slider_1=e.target.value;

        if (this.acLayer==="light"){
          this.slideBreaks=2.85;
        console.log(e.target.value/this.slideBreaks);
        this.sfilter = this.sliderLabels[1].light_polution[Math.round(e.target.value/this.slideBreaks)];
        this.url='http://'+window.location.hostname+':5000/state/'+this.sfilter+'/light/{z}/{x}/{y}';
      }

       else {
           this.slideBreaks=9;
        this.sfilter = this.sliderLabels[0].seasonal_trend[e.target.value/this.slideBreaks];
        this.url='http://'+window.location.hostname+':5000/state/'+this.sfilter+'/'+this.acLayer+'/{z}/{x}/{y}';
       }
       //ndvi, evi, ari, slavi
       //ari --> postaviti varijablu za layere
       //light polutin light

       console.log(this.url);
     },

     get_basmap_url() {
         return 'http://'+window.location.hostname+':5000/basemap/{z}/{x}/{y}'
     },

     slideCompare: function(e){

       //this.slider_2=e.target.value;
       //console.log(this.slider_2);
       //console.log(this.slider_1);
       //console.log(this.sliderLabels[0].seasonal_trend[e.target.value/9]);
      this.sfilter2 = this.sliderLabels[0].seasonal_trend[e.target.value/9];
      //ndvi, evi, ari, slavi
      //ari --> postaviti varijablu za layere
      this.url='http://'+window.location.hostname+':5000/diff/'+this.sfilter+'/'+this.sfilter2+'/'+this.acLayer+'/{z}/{x}/{y}';
      //console.log(this.url);
    },

     viewFilter: function(e){
       this.show_legend = false;
      this.acLayer = e.target.value;
      //ndvi, evi, ari, slavi
      //ari --> postaviti varijablu za layere
      //state=diff, 2018_s1, 2018_s10
      if (this.tempCompare===true){
      this.url='http://'+window.location.hostname+':5000/diff/'+this.sfilter+'/'+this.sfilter2+'/'+this.acLayer+'/{z}/{x}/{y}';}
      else {
      //this.url='http://'+window.location.hostname+':5000/state/'+this.sfilter+'/'+this.acLayer+'/{z}/{x}/{y}';
      if (this.acLayer==="light"){
        this.slideBreaks=2.85;
      //console.log(this.sliderLabels[1].light_polution[(e.target.value/this.slideBreaks).round()]);
      this.sfilter = '201812'
      this.url='http://'+window.location.hostname+':5000/state/'+this.sfilter+'/light/{z}/{x}/{y}';
    }
    else if(this.singleMaps.includes(this.acLayer)){
      this.slideBreaks=1;
      this.url='http://'+window.location.hostname+':5000/maps/'+this.acLayer+'/{z}/{x}/{y}';
      this.legend_url='http://'+window.location.hostname+':5000/legend/'+this.acLayer;
      this.show_legend = true;
    }

     else {
         this.slideBreaks=9;
      this.sfilter = '2018_s4'
      this.url='http://'+window.location.hostname+':5000/state/'+this.sfilter+'/'+this.acLayer+'/{z}/{x}/{y}';
     }
      }
      console.log(this.url);
    },
    changeView: function(e){
      this.tempCompare=e.target.checked;
    }
  },

    });

    // center of the map
/*var center = [-33.8650, 151.2094];

// Create the map
var map = L.map('mapid').setView(center, 3);

// Set up the OSM layer
L.tileLayer(
  'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18
  }).addTo(map);

// add a marker in the given location
L.marker(center).addTo(map);*/
}

document.addEventListener('DOMContentLoaded', (event) => do_it())
