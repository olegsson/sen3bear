from pathlib import Path

raster_dir = Path('/data/hackathon/season')
tilecache_dir = raster_dir.joinpath('../tilecache')
basemap_dir = raster_dir.joinpath('../basemap')
light_tif = raster_dir.joinpath('../svjetlost_htrs.tif')
maps_dir = raster_dir.joinpath('../maps')

color_levels = 100
