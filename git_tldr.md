## Naredbe i to

`git pull`
- povlači promjene u lokalnu granu

`git branch <grana>`
- otvara novu granu iz trenutne

`git checkout <grana>`
- prebacuje na drugu granu

`git add <fajlovi>`
- dodaje sve navedene fajlove i direktorije i promjene u njima u commit

`git commit`
- sprema dodane promjene u novi commit (i otvara text editor za commit message)

`git commit -a -m "<poruka>"`
- one liner za commitati promjene u svim fajlovima (koji nisu `untracked`) i napisati commit message

`git push`
- šalje promjene iz lokalne grane na GitLab

`git push -u origin <grana>`
- za slučaj da lokalna grana ne postoji na GitLabu

`git merge <grana>`
- dodaje promjene iz odabrane grane u trenutnu (kao novi commit)

`git stash`
- stasha promjene i vraća granu na zadnji commit

`git stash pop`
- aplicira stashane promjene u trenutnu granu

`git stash drop` / `git stash clear`
- briše stash


*dodavat ćemo još po potrebi :)*


## GUI alternative

#### ungit
https://github.com/FredrikNoren/ungit

#### git-cola
https://github.com/git-cola/git-cola

#### GitKraken
https://www.gitkraken.com/
- *(vjerojatno najbolji, ali besplatna verzija ovdje nije opcija jer ne podržava privatne repozitorije)*

#### editori s git integracijom
- atom
- VS Code
- ...


## Dodatno

#### najbolji git intro na svijetu
http://tkrajina.github.io/uvod-u-git/git.pdf
