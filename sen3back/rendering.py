from colour import Color
import numpy as np
from PIL import Image
from io import BytesIO

try:
    from . import settings
except ImportError:
    from sen3back import settings

def rgba(clr):
    return np.array(list(clr.rgb) + [1.]) * 255

color_ramp_vege = np.array([
    rgba(c) for c in Color('black').range_to('#228B22', settings.color_levels)
]).astype(np.uint8)

color_ramp_stress = np.array([
    rgba(c) for c in Color('#228B22').range_to('purple', settings.color_levels)
]).astype(np.uint8)

color_ramp_diff_vege = np.array([
    rgba(c) for c in Color('orange').range_to('black', settings.color_levels//2)
] + [
    rgba(c) for c in Color('black').range_to('#228B22', settings.color_levels//2)
]).astype(np.uint8)

color_ramp_diff_stress = color_ramp_diff_vege[::-1]

color_ramp_light = np.array([
    rgba(c) for c in Color('black').range_to('white', settings.color_levels)
# ] + [
#     rgba(c) for c in Color('yellow').range_to('white', settings.color_levels//2)
# ] + [
#     rgba(c) for c in Color('orange').range_to('white', settings.color_levels//2)
# ] + [
#     rgba(c) for c in Color('yellow').range_to('white', settings.color_levels//4)
]).astype(np.uint8)

def render(arr, vmin=0, vmax=1, color_ramp=color_ramp_vege):
    if isinstance(color_ramp, str):
        color_ramp = globals()['color_ramp_'+color_ramp]
    nodata = np.isnan(arr)
    arr = np.minimum(vmax, np.maximum(vmin, arr))
    color_index = ((arr - vmin) / (vmax - vmin) * (color_ramp.shape[0] - 1)).astype(np.uint8)
    arr = color_ramp[color_index]
    arr[nodata] = 0
    buffer = BytesIO()
    # print(arr)
    img = Image.fromarray(arr, mode='RGBA')
    img.save(buffer, format='PNG')
    return buffer.getvalue()
