#%%
#import os
#os.environ["CUDA_VISIBLE_DEVICES"]="-1"

import rasterio as rio
#from traingen import ChooChooTrain
import tensorflow as tf
#import tensorflow
#config = tf.ConfigProto()
#config.gpu_options.allow_growth = True

from tensorflow.keras import layers, models, optimizers
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint
from sklearn import model_selection, metrics
from sklearn.model_selection import train_test_split
from pathlib import Path
import numpy as np
from traingen import ChooChooTrain
from munch import Munch


def sample_classes_idx(y_all, sample_rate):
    ind=np.ones_like(len(y_all),dtype=bool )
    for c,rc in enumerate(sample_rate):
        ind = ind & ((y_all != c) | ((y_all==c) & (np.random.rand(len(y_all))<rc)))
    return ind

def get_x(x_all, idx):
    xx = np.c_[x_all[idx,:-1]/5000,np.eye(4)[season[idx]-1]]
    return xx

def get_y(y_all, idx):
    
    nclasses = y_all.max() #len(np.unique(yy))
    yy = y_all[idx]
    eye = np.eye(nclasses)
    yy=eye[yy-1]

    return yy

#%%
fld = Path(r'D:\DATA\HACKATON\train')
fld = Path('/data/hackathon/train')
fld_nn = fld/'nn'

prefix='clc8'

fn_data = fld/'mosaics_data.npz'
# data = np.load(fn_data)
with np.load(fn_data) as data:
    y = data['y']
    ind = sample_classes_idx(y, [0.5,0.4,1,1,0.5,0.2,1,1])
    y=y[ind]
    x = data['x'][ind]
    season = data['season'][ind]

random_state=42
train_size=0.8
test_size=0.1
validation_size=0.1
indices = np.arange(x.shape[0])
idx_train, idx_test = train_test_split(indices, test_size=test_size, 
    random_state=random_state, shuffle=True, stratify=y)

#idx_train, idx_verification = train_test_split(idx_train, test_size=validation_size,
#    random_state=random_state, shuffle=True, stratify=y[idx_train])
classes = np.unique(y)
nclasses = len(classes)

np.random.shuffle(idx_train)
#x_train = np.c_[x[idx_train,:-1]/10000,np.eye(4)[season[idx_train]-1]]
x_train = get_x(x, idx_train)
y_train = get_y(y, idx_train)

nchannels = x_train.shape[1]

nepochs=30


#%%
def get_model(opt):
#%%
    input_kernel_size = 1
    model = models.Sequential()
    
    input_shape =  (nchannels,)

    #in1 = layers.Conv1D()
    #in2 = layers.Input()
    #l = layers.Concatenate(in1, in2)

    #model.add(layers.Conv1D(16, 3, input_shape=input_shape))
    model.add(layers.Dense(32, activation='relu', input_shape=input_shape))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(0.2))
    model.add(layers.Dense(64, activation='relu'))
    #model.add(layers.BatchNormalization())
    model.add(layers.Dropout(0.2))
    #model.add(layers.BatchNormalization())
    model.add(layers.Dense(128, activation='relu'))
    model.add(layers.Dropout(0.2))
    model.add(layers.Dense(256, activation='relu'))
    model.add(layers.Dropout(0.2))
    model.add(layers.Dense(256, activation='relu'))
    model.add(layers.Dropout(0.2))
    model.add(layers.Dense(128, activation='relu'))
    model.add(layers.Dropout(0.2))
    #model.add(layers.BatchNormalization())
    # model.add(layers.Dense(256, activation='relu'))
    # model.add(layers.BatchNormalization())
    #model.add(layers.Dense(64, activation='relu'))
    #model.add(layers.BatchNormalization())
    #model.add(layers.Dense(32, activation='relu'))
    #model.add(layers.BatchNormalization())
    #model.add(layers.Dense(16, activation='relu'))
    #model.add(layers.BatchNormalization())

    model.add(layers.Dense(nclasses, activation='sigmoid', name='sigmoid'))

    model.add(layers.Dense(nclasses, activation='softmax'))

    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()
#%%
    model.save(fld_nn/f'{prefix}_{opt.ver}_model.h5')    
    return model

def training(model):
    #log_dir = str(fld/'nn')
    #tensorboard = TensorBoard(log_dir=log_dir, write_graph=True)
    checkpoint = ModelCheckpoint(filepath = str(fld_nn/f'{prefix}_{opt.ver}.h5'), 
        save_best_only=True, save_weights_only=False, monitor='val_loss', verbose=1)
    #print(f'tensorboard --logdir={log_dir}')

    #trainsteps, gtrain = msp.train(batch_size=opt.batch_size)
    #validationsteps, gvalidation = msp.validation(batch_size=opt.batch_size)

    model.fit(x=x_train, y=y_train, validation_split=validation_size, use_multiprocessing=True, 
            epochs=nepochs, verbose=1, callbacks=[checkpoint] )

def load_best(opt):
    model = models.load_model(str(fld/'nn'/f'{prefix}_{opt.ver}_model.h5'))
    model_fn = str(fld/'nn'/f'{prefix}_{opt.ver}.h5')
    #model = models.load_model(model_fn)
    model.load_weights(model_fn)
    #model = models.load_model(model_fn)
    return model

def test(model):
     # tesstiranje
    x_test = get_x(x, idx_test) # np.c_[x[idx_test],np.eye(4)[season[idx_test]-1]]
    #x_test = x[idx_test]
    prd = model.predict(x_test)
    prd_test = prd.argmax(axis=-1)+1

    y_test = y[idx_test]

    ind = prd_test>0        # nije mi baš jasno ...
    y_test = y_test[ind]
    prd_test = prd_test[ind]
    report = metrics.classification_report(y_test, prd_test)
    confusion = metrics.confusion_matrix(y_test, prd_test)
    print(report)
    print(confusion)

    sf = fld_nn/f'{prefix}_{opt.ver}_stats.txt'
    sf.write_text(f'{report}\n{repr(confusion)}')

#%%
def prediction(opt,model,year, season):
    #fn_input = r'D:\DATA\HACKATON\mosaics\season\mosaic_2018_s2.tif'
    #fn_input = '/data/hackathon/mosaics/season/mosaic_2018_s2.tif'
    fn_input = f'/data/hackathon/mosaics/season/mosaic_{year}_s{season}.tif'
    fn_roi = r'D:\DATA\HACKATON\mosaics\roi\ROI_10am.shp'
    fn_roi = '/data/hackathon/ROI_10am.shp'
    msp = ChooChooTrain.from_msp_1file(fn_input, fn_roi, 'ID', 0)

    img = np.zeros(list(msp.input.mask.shape)+[nclasses+1], dtype='uint8').reshape((-1,nclasses+1))
    #img1 = np.zeros_like(img)+255

    season_inp = np.zeros((1,4))
    season_inp[0,season-1]=1
    prdsteps, gprd = msp.prediction(batch_size=100000)
    for i in range(prdsteps):
        if i%10==0:
            print(f'{i}/{prdsteps}')
        idx, x = next(gprd)
        x = x.squeeze()[:,:-1]/5000
        x = np.c_[x, np.tile(season_inp, (x.shape[0],1))]
        prd = model.predict_on_batch(x)#.numpy()
        img[idx,1:] = prd*100  
        prd = prd.argmax(axis=-1)
        img[idx,0] = prd+1 #msp.target.encoder.inverse_transform(prd)

    img = img.reshape(list(msp.input.mask.shape)+[nclasses+1])
    profile = rio.default_gtiff_profile
    profile.update(nodata=0, dtype='uint8', transform=msp.input.transform, crs=msp.input.crs,
        width=img.shape[1], height=img.shape[0], count=nclasses+1)

    outfile = fld/f'prd_{prefix}_v{opt.ver}_{year}_s{season}.tif'
    with rio.open(outfile, mode='w', **profile) as r:
        for i in np.arange(nclasses+1):
            r.write(img[:,:,i].astype(rio.uint8),int(i+1))

def predict_all(opt):
    model = load_best(opt)
    for year in [2016,2107,2018,2019]:
        for season in [1,2,3,4]:
            prediction(opt, model, year, season)

def predict_postprocess_year(year):
    #/data/hackathon/train/prd_clc8_vtest4_2018_s4.tif
    fns = list(Path(f'/data/hackathon/train/').glob(f'prd_clc8_vtest4_{year}_s*.tif'))
    fns = ['/data/hackathon/train/prd_clc8_vtest4_2018_s3.tif',\
    ('/data/hackathon/train/prd_clc8_vtest4_2018_s1.tif'),\
    ('/data/hackathon/train/prd_clc8_vtest4_2018_s2.tif')]
    data = None
    for fn in fns:
        with rio.open(fn) as src:
            profile = src.profile
            img = np.moveaxis(src.read().astype(float),0,-1)[:,:,1:]
            img = np.nan_to_num(img)
            if data is None:
                data = img[:,:,0:]
            else:
                data = data + img[:,:,0:]
    prd = data.argmax(axis=-1)+1

    profile.update(nodata=0, dtype='uint8',count=1)
    fn_out = '/data/hackathon/train/2018_pp_all_nowinter.tif'
    with rio.open(fn_out,'w',**profile) as dst:
        dst.write(prd.astype(rio.uint8),1)

#%%
if __name__=='__main__':
    # test1 - bez sezone
    # test2 - sa sezonom na kraju (4 dummyija)
    # test3 - bez count-a
    # test4 - smanjene klase 1,2,4,5 ( [0.5,0.5,1,1,0.5,0.5,1,1])
    # test5 - šira i ne tako duboka mreža
    opt=Munch(ver='test5')
    #model = get_model(opt)
    #training(model)
    model = load_best(opt)
    test(model)
    #predict_all(opt)