#%%
import rasterio as rio
from rasterio.features import rasterize
from pathlib import Path
import numpy as np
from functools import reduce
from operator import and_
import geopandas as gp
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

class InputSource:

    def __init__(self, sources, mask=None, window_radius=5, scale=1):
        self.sources = sources
        self.shape = None
        self.nbands = len(sources)
        self.mask = None if callable(mask) else mask
        self.scale = scale
        data = None

        for b, source in enumerate(sources):            
            with rio.open(source['filename']) as src:
                if self.shape is None:
                    self.shape = src.shape
                    self.transform = src.transform
                    self.crs = src.crs
                    data = np.zeros(list(self.shape) + [self.nbands])
                else:
                    if self.shape != src.shape:
                        raise Exception("Sources don't have equal geometry!")

                if self.mask is None:
                    self.mask = src.dataset_mask()>0
                else:
                    self.mask = self.mask & (src.dataset_mask()>0)

                data[:,:,b] = src.read(source['bandnumber'])
        
        if callable(mask):
            self.mask = self.mask & mask(data).sum(axis=-1).astype(bool)
        self.data = data
        self.indices = np.nonzero(self.mask.flat)[0]

        #self.sources = [rio.open(f) for f in files]
        #self.shape = self.sources[0].shape
        self.window_radius = window_radius
        self.tile_size = int(2 * window_radius + 1)
        
        #self.data = np.stack((s.read()
        #    for s in self.sources), axis=-1).reshape(*self.shape, -1)
        
        self.tile_shape = (self.tile_size, self.tile_size, self.nbands)
        self.empty_tile = np.ones(self.tile_shape) * np.nan

        

    def get_tile(self, idx):
        cy = idx // self.shape[1]
        cx = idx % self.shape[1]

        return self.get_tile_cxy(cy, cx)

    def get_tile_cxy(self, cy, cx):
        # Zašto ne vraćati None?
        ty0, tx0 = cy - self.window_radius, cx - self.window_radius
        if tx0 < 0 or ty0 < 0:
            return self.empty_tile
        
        ty1, tx1 = ty0 + self.tile_size, tx0 + self.tile_size
        if ty1 > self.shape[0] or tx1>self.shape[1]:
            return self.empty_tile

        tile_mask = self.mask[ty0:ty1, tx0:tx1]
        if not tile_mask.all():
            return self.empty_tile

        return self.scale * self.data[ty0:ty1, tx0:tx1, :]

class TargetSource:

    def __init__(self, input_file, label_column,
        shape, transform, data_mask=None, fill=-1,
        train_size=None, test_size=None, validation_size=None,
        random_state=None, shuffle=True, supress_classes=None):

        self.train_size=train_size
        self.test_size=test_size
        self.validation_size=validation_size
        self.random_state=random_state

        self.nodata = fill
        self.df = gp.read_file(input_file)
        
        if supress_classes is not None:
            self.df = self.df.loc[~self.df[label_column].isin(supress_classes)]
        
        self.encoder = LabelEncoder()
        labels = self.df[label_column].values
        self.encoder.fit(labels)
        shapes = zip(self.df.geometry.values,
            self.encoder.transform(labels))        
        data = rasterize(shapes, out_shape=shape, fill=fill, transform=transform, dtype='int16')
        self.classes = self.encoder.transform(self.encoder.classes_)
        #iy, ix = np.where(data >= 0)
        #indices = np.stack((iy, ix), axis=-1)
        self.mask = data != fill
        if data_mask is not None:
            self.mask = self.mask & data_mask
        indices = np.nonzero(self.mask.flat)[0]
        target = data.flat[indices]
        #np.eye(np.unique(labels).size)[data[iy, ix]]
        self.indices = indices
        self.target = target
        
        idx_train, self.idx_test, y_train, self.y_test = train_test_split(
            indices, target, test_size=test_size,
            train_size=train_size, random_state=random_state,
            shuffle=True, stratify=target)

        self.idx_train, self.idx_validation, self.y_train, self.y_validation = train_test_split(
            idx_train, y_train, test_size=validation_size, random_state=random_state, 
            shuffle=True, stratify=y_train
        )

    def equalize_classes(self):
        # Postavi sve klase na podjednaki broj za treniranje i testiranje
        target = np.concatenate([self.y_train, self.y_test, self.y_validation])
        indices = np.concatenate([self.idx_train, self.idx_test, self.idx_validation])
        
        bc = np.bincount(target)
        bc = bc.min()/bc    #postotak uzimanja

        ind=np.ones_like(indices,dtype=bool )
        for c,rc in enumerate(bc):
            ind = ind & ((target != c) | ((target==c) & (np.random.rand(len(target))<rc)))

        idx_train, self.idx_test, y_train, self.y_test = train_test_split(
            indices[ind], target[ind], test_size=self.test_size,
            train_size=self.train_size, random_state=self.random_state,
            shuffle=True, stratify=target[ind])

        self.idx_train, self.idx_validation, self.y_train, self.y_validation = train_test_split(
            idx_train, y_train, test_size=self.validation_size, random_state=self.random_state, 
            shuffle=True, stratify=y_train
        )

    def count_valid_class_train(self, class_id):
        # counts number of valid training cases with specific class_id
        return (self.y_train==self.encoder.transform([1])[0]).sum()

    def count_valid_class_test(self, class_id):
        return (self.y_test==self.encoder.transform([1])[0]).sum()


    
class ChooChooTrain:

    @classmethod
    def from_rgb(cls, input_file:str, target_file, target_column, window_radius, train_size=None, 
        test_size=None, validation_size=None, supress_classes=None):
        sources = [dict(filename=input_file, bandname=b, bandnumber=i)
            for i,b in zip([1,2,3],['red','green','blue'])]
        return cls(sources, target_file, target_column, window_radius=window_radius,
            train_size=train_size, test_size=test_size, validation_size=validation_size,
            supress_classes=supress_classes, scale=1.0/255.0)

    @classmethod
    def from_msp(cls, input_file:str, target_file, target_column, window_radius, train_size=None, 
        test_size=None, validation_size=None, supress_classes=None):
        input_path = Path(input_file)
        folder = input_path.parent

        prefix = '_'.join(input_path.name.split('_')[:-1])
        sources=[]
        for fn in folder.glob(prefix+'*.tif'):
            source=dict(filename=fn, bandname=fn.stem.split('_')[-1], bandnumber=1)
            sources.append(source)

        return cls(sources, target_file, target_column, window_radius=window_radius,
            train_size=train_size, test_size=test_size, validation_size=validation_size,
            supress_classes=supress_classes)
        
    @classmethod
    def from_msp_1file(cls, input_file: str, target_file, target_column, window_radius, train_size=None, 
        test_size=None, validation_size=None, supress_classes=None):

        sources=[]
        with rio.open(input_file) as src:
            profile = src.profile
            cnt = profile['count']
            for c in range(cnt):
                source=dict(filename=input_file, bandname=f'b{c+1}', bandnumber=c+1)
                sources.append(source)
        
        return cls(sources, target_file, target_column, window_radius=window_radius,
            train_size=train_size, test_size=test_size, validation_size=validation_size,
            supress_classes=supress_classes)



    def __init__(self, sources, target_file, label_column,
        mask=None, window_radius=5,
        fill=-1, train_size=None, test_size=None, validation_size=None,
        random_state=None, shuffle=True, supress_classes=None, scale=1):

        self.shuffle = shuffle
        self.input = InputSource(sources, mask, window_radius, scale=scale)

        refsrc = self.input.shape
        self.target = TargetSource(target_file, label_column,
            self.input.shape, self.input.transform, self.input.mask, fill, train_size,
            test_size, validation_size, random_state, shuffle, supress_classes=supress_classes)

    def _generate(self, idx, y, batch_size=64, shuffle=False, steps=None):
        steps = steps or y.shape[0]//batch_size
        ind = np.arange(len(idx))
        eye = np.eye(self.target.encoder.classes_.size)
        while True:
            if shuffle:
                np.random.shuffle(ind)
            for i in range(steps):
                batch_start, batch_end = i * batch_size, (i + 1) * batch_size
                idx_batch = idx[ind[batch_start:batch_end]]
                y_batch = eye[y[ind[batch_start:batch_end]]]
                x_batch = np.stack([self.input.get_tile(idx) for idx in idx_batch])
                valid = ~np.isnan(x_batch.reshape(x_batch.shape[0], -1)).any(axis=-1)

                yield x_batch[valid], y_batch[valid]

    def _generate_x(self, idx, batch_size, steps=None):
        steps = steps or idx.size//batch_size
        for i in range(steps):
            batch_start, batch_end = i* batch_size, (i+1) * batch_size
            idx_batch = idx[batch_start:batch_end]
            x_batch = np.stack([self.input.get_tile(idx) for idx in idx_batch])
            valid = ~np.isnan(x_batch.reshape(x_batch.shape[0], -1)).any(axis=-1)

            yield idx_batch[valid], x_batch[valid]

    def _generate_oneclass(self, class_id, batch_size, shuffle, steps):
        pass
        # Treba proći sve self.target.idx_train koji su za class_id i za svaki taj dodati još jedan slučajni case koji je 
        # bilo gdje ?
        
    def train_oneclass(self, class_id, batch_size=64):
        steps = self.target.count_valid_class_train*2//batch_size
        generator = self._generate_oneclass(class_id, batch_size, self.shuffle, steps)

    def train(self, batch_size=64):
        steps = self.target.y_train.shape[0]//batch_size
        return steps, self._generate(self.target.idx_train,
            self.target.y_train, batch_size, self.shuffle, steps)

    def test(self, batch_size=64):
        steps = self.target.y_test.shape[0]//batch_size
        return steps, self._generate(self.target.idx_test,
            self.target.y_test, batch_size, False, steps)

    def validation(self, batch_size=64):
        steps = self.target.y_validation.shape[0]//batch_size
        return steps, self._generate(self.target.idx_validation,
            self.target.y_validation, batch_size, False, steps)

    def prediction(self, batch_size=64):
        steps = self.input.indices.size//batch_size
        return steps, self._generate_x(self.input.indices, batch_size, steps)

    def get_train_data(self):
        ind = self.target.idx_train.copy()
        np.random.shuffle(ind)
        x = np.stack([self.input.get_tile(idx) for idx in ind])
        y = self.target.y_train
        return x,y
    
    def get_test_data(self):
        ind = self.target.idx_test.copy()
        np.random.shuffle(ind)
        x = np.stack([self.input.get_tile(idx) for idx in ind])
        y = self.target.y_test
        return x,y

    def get_validation_data(self):
        ind = self.target.idx_validation.copy()
        np.random.shuffle(ind)
        x = np.stack([self.input.get_tile(idx) for idx in ind])
        y = self.target.y_validation
        return x,y

    def get_all_data(self):
        ind = self.target.indices
        x = np.stack([self.input.get_tile(idx) for idx in self.target.indices])
        y = self.target.encoder.inverse_transform(self.target.target)
        return ind,x,y



#%%

if __name__ == '__main__':

    def testit():
        testdir = Path('/data/work/hac')
        inputfile = next(testdir.glob('tiffs/1_1*.tif')).as_posix()
        targetfile = next(testdir.glob('roi/1_1*.shp')).as_posix()

        ds = ChooChooTrain(inputfile, targetfile, 'C_ID', lambda x: x>=0)

        trainsteps, gtrain = ds.train()
        teststeps, gtest = ds.test()
        
        for __ in range(trainsteps*2):
            x, y = next(gtrain)

        for __ in range(teststeps*2):
            x, y = next(gtest)

    from cProfile import run
    run('testit()')


