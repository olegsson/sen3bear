#%%
import rasterio as rio
from pathlib import Path
import geopandas
from traingen import ChooChooTrain
import numpy as np

fld = Path(r'D:\DATA\HACKATON')
fld = Path('/data/hackathon')
#%%
fld_data = fld/'mosaics'/'season'
fn_roi = r'D:\DATA\HACKATON\mosaics\roi\ROI_10am.shp'
fn_roi = '/data/hackathon/ROI_10am.shp'
label_column = 'ID'

#%% Pokupimo mozaike 
bad_files=['2015_s3','2016_s1','2016_s2']
fns = filter(lambda s: sum(map(lambda x: x in s.name, bad_files))==0, fld_data.glob('*.tif'))
fns = list(fns)


#%%
xx=[]; yy=[]; iind=[]; yyear=[]; sseason=[]
for fn in fns:
    print(fn)
    _,_y,_s = fn.with_suffix('').name.split('_')
    year=int(_y)
    season=int(_s.strip('s'))
    
    input = ChooChooTrain.from_msp_1file(fns[0], fn_roi, label_column, 0)
    ind, x, y = input.get_all_data()
    xx.append(x)
    yy.append(y)
    iind.append(ind)
    yyear.append(np.zeros_like(ind) + year)
    sseason.append(np.zeros_like(ind) + season)

ind=np.concatenate(iind)
x=np.concatenate(xx).squeeze()
y=np.concatenate(yy)
year=np.concatenate(yyear)
season=np.concatenate(sseason)
outfn = fld/'train'/'mosaics_data.npz'
np.savez(outfn, ind=ind, x=x, y=y, year=year, season=season)

'''
ng, gen = input.train(10000)
xx=[]; yy=[]
for i,(x,y) in enumerate(gen):
    print(i)
    xx.append(x)
    yy.append(y)
xx=np.array(xx)
yy=np.array(yy)

'''
    

#%%


#%%
