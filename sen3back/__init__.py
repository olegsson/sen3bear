from . import app

for v in filter(lambda v: v.startswith('api_'), app.__dict__):
    exec(f'global {v}; from sen3back.app import {v}')
