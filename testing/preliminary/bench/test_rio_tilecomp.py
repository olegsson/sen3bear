from glob import glob
import cProfile
from shapely import speedups
if speedups.available:
    speedups.enable()
from shapely.geometry import Point, Polygon, box
import numpy as np
import rasterio as rio
from rasterio.windows import from_bounds
from rasterio.enums import Resampling
from random import choice

from .test_rio_point import random_coords
from .test_rio_tile import get_tile_data
from . import settings


def test():
    src1 = rio.open(choice(settings.raster_files()))
    src2 = rio.open(choice(settings.raster_files()))
    src3 = rio.open(choice(settings.raster_files()))
    pt = random_coords(src1)
    tile1 = get_tile_data(src1, pt)
    tile2 = get_tile_data(src2, pt)
    tile3 = get_tile_data(src3, pt)
    return (tile1+tile2)/tile3

if __name__ == '__main__':
    print(test())
