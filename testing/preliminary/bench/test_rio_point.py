import rasterio as rio
from glob import glob
import cProfile
from shapely import speedups
if speedups.available:
    speedups.enable()
from shapely.geometry import Point, Polygon, box
import numpy as np

from . import settings

def random_coords(src):
    return [
        np.random.uniform(src.bounds[0], src.bounds[2]-src.width*src.transform[0]*.5),
        np.random.uniform(src.bounds[1], src.bounds[3]-src.height*src.transform[0]*.5),
    ]

def get_point_data(filename):
    src = rio.open(filename)
    return [s[0] for s in src.sample([random_coords(src)])]

def test():
    files = settings.raster_files()
    [*results] = map(get_point_data, files)
    return results

if __name__ == '__main__':
    print(test())
