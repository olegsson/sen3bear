from flask import Flask, Response
import json
from PIL import Image
from io import BytesIO
import numpy as np
from rio_tiler import main
from os import path
from random import choice

from . import test_rio_point as riopoint
from . import test_rio_tile as riotile
from . import test_rio_tilecomp as riotilecomp
from . import settings

app_point = Flask('point_api')
app_tile = Flask('tile_api')
app_tilecomp = Flask('tilecomp_api')
app_rio_tiler = Flask('rio_tiler_api')

@app_point.route('/')
def point():
    return json.dumps([str(x) for x in riopoint.test()])

@app_tile.route('/', defaults={'path': ''})
@app_tile.route('/<path:path>')
def tile(path):
    data = riotile.test()
    rmax = data.max() - data.min()
    data = (255 * (data - data.min()) / rmax).astype(np.uint8)
    buffer = BytesIO()
    Image.fromarray(data).save(buffer, format='png')
    return Response(buffer.getvalue(), mimetype='image/png')

@app_tilecomp.route('/', defaults={'path': ''})
@app_tilecomp.route('/<path:path>')
def tilecomp(path):
    data = riotilecomp.test()
    rmax = data.max() - data.min()
    data = (255 * (data - data.min()) / rmax).astype(np.uint8)
    buffer = BytesIO()
    Image.fromarray(data).save(buffer, format='png')
    return Response(buffer.getvalue(), mimetype='image/png')

@app_rio_tiler.route('/<int:z>/<int:x>/<int:y>.png')
def rio_tile(x, y, z):
    fileurl = 'file://' + path.realpath(choice(settings.raster_files()))
    data, mask = main.tile(fileurl, x, y, z)
    data = choice(data)
    rmax = data.max() - data.min()
    data = (255 * (data - data.min()) / rmax).astype(np.uint8)
    buffer = BytesIO()
    Image.fromarray(data).save(buffer, format='png')
    return Response(buffer.getvalue(), mimetype='image/png')
