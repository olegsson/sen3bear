def b2i(band): # sentinel2 band to tile index
    return {
        2: 0,
        3: 1,
        4: 2,
        5: 3,
        6: 4,
        7: 5,
        8: 6,
        80: 7,
        9: 8,
        11: 9,
        12: 10,
    }[band]

def timestamp2year(timestamp):
    year, season = map(float, timestamp.split('_s'))
    return year + .125 + season * .25

def time2idx(t):
    year, month = map(int, (t[:4], t[4:]))
    dy = year - 2014
    dm = month % 12
    return dy * 12 + dm
