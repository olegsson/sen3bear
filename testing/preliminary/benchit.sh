#/usr/bin/env bash

python -m timeit -n 10 -r 10 "from bench.test_$1 import test; test()" | tee bench/logs/$1.log
