from .utils import b2i

def func_ndvi(arr):
    return (arr[b2i(8)] - arr[b2i(4)]) / (arr[b2i(8)] + arr[b2i(4)]), 0., 1., 'vege'

def func_ari(arr):
    return 1.0 / arr[b2i(3)] - 1.0 / arr[b2i(5)], -10., 10., 'stress'

def func_slavi(arr):
    return arr[b2i(8)] / (arr[b2i(4)] + arr[b2i(12)]), 0., 3., 'vege'

def func_evi(arr):
    return 2.5 * (arr[b2i(8)] - arr[b2i(4)]) / ((arr[b2i(8)] + 6. * arr[b2i(4)] - 7.5 * arr[b2i(2)]) + 1.), -1., 1., 'vege'
