#%%
import os
os.environ["CUDA_VISIBLE_DEVICES"]="-1"
from tensorflow.keras import models
import numpy as np


model_fn = '/data/hackathon/train/nn/clc8_test5_model.h5'
model_weights_fn = '/data/hackathon/train/nn/clc8_test5.h5'
model = models.load_model(model_fn)
model.load_weights(model_fn)

season_inp = np.zeros((1,4))

def predict_tile(img, season):
    img = img[:,:,:-1]*2  # ja dijelim sa 5000, a dobivam podjeljeno sa 10000
    width, height, _ = img.shape
    img = img.reshape((width*height,-1))

    season_inp[:] = 0
    season_inp[0,season-1]=1

    x = np.c_[img, np.tile(season_inp, (img.shape[0],1))]
    prd = model.predict(x).argmax(axis=-1)

    return prd+1
#%%
if __name__=='__main__':
    # test
#%%
    import rasterio as rio
    from rasterio.windows import Window

    with rio.open('/data/hackathon/mosaics/season/mosaic_2018_s2.tif') as src:
        window = Window(src.profile['width']//2, src.profile['height']//2, 512, 512)
        tile = src.read(window = window)/10000
        tile = np.moveaxis(tile, 0, -1)

    klass = predict_tile(tile, 2)
    print(klass.min(), klass.max())
# %%
